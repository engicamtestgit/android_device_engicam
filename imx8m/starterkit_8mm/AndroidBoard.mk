LOCAL_PATH := $(call my-dir)

include device/engicam/common/build/kernel.mk
include device/engicam/common/build/uboot.mk
include device/engicam/common/build/dtbo.mk
include device/engicam/common/build/imx-recovery.mk
include device/engicam/common/build/gpt.mk
include $(LOCAL_PATH)/AndroidUboot.mk
include $(LOCAL_PATH)/AndroidTee.mk
include $(FSL_PROPRIETARY_PATH)/fsl-proprietary/media-profile/media-profile.mk
include $(FSL_PROPRIETARY_PATH)/fsl-proprietary/sensor/fsl-sensor.mk
